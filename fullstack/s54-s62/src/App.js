import AppNavbar from './components/AppNavbar';
import Courses from './pages/Courses';
import Home from './pages/Home';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
// Bootstrap

// 
import { Container } from 'react-bootstrap';
import './App.css';


import Register from './pages/Register'

function App() {
  return (
    <>
      <AppNavbar />
      <Container>
        <Home />
        <Courses />
        <Register />
      </Container>
    </>
  );
}

export default App;
