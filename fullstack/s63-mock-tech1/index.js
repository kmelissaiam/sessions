function countLetter(letter, sentence) {
  let result = 0;
  if (letter.length === 1) {
    for (let i = 0; i < sentence.length; i++) {
      if (sentence[i] === letter) {
        result++;
      }
    }
    return result;
  }
}

function isIsogram(text) {
  for (let i = 0; i < text.length; i++) {
    if (text[i] === text[i + 1]) {
      return false;
    }
  }
  return true;
}

function purchase(age, price) {
  if (age < 13) {
    return undefined;
  } else if (age >= 13 && age <= 21) {
    return String(Math.round(price * 0.8 * 100) / 100);
  } else if (age >= 65) {
    return String(Math.round(price * 0.8 * 100) / 100);
  } else if (age >= 22 && age <= 64) {
    return String(Math.round(price * 100) / 100);
  } else {
    return false;
  }
}

function findHotCategories(items) {
  const result = items.filter((e) => e.stocks === 0).map((e) => e.category);
  return result.filter((i, index) => result.indexOf(i) === index);
}

function findFlyingVoters(candidateA, candidateB) {
  let result = [];
  for (let i = 0; i < candidateA.length; i++) {
    for (let j = 0; j < candidateB.length; j++) {
      if (candidateA[i] === candidateB[j]) {
        result.push(candidateA[i]);
      }
    }
  }
  return result;
}

module.exports = {
  countLetter,
  isIsogram,
  purchase,
  findHotCategories,
  findFlyingVoters,
};
