const express = require('express');
// Allows access to HTTP Methods and Middlewares
const router = express.Router();
const taskController = require('../controllers/taskController.js');

// Get All Tasks
router.get('/', (req, res) => {
  taskController.getAllTask().then((resultFromController) => res.send(resultFromController));
});

// Create Tasks
router.post('/', (req, res) => {
  taskController.createTask(req.body).then((resultFromController) => res.send(resultFromController));
});

// Delete Tasks using wildcard on params
router.delete('/:id', (req, res) => {
  taskController.deleteTask(req.params.id).then((resultFromController) => res.send(resultFromController));
});

// Update a task
router.put('/:id', (req, res) => {
  taskController
    .updateTask(req.params.id, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//Search a specific task
router.get('/:id', (req, res) => {
  taskController
    .getSpecificTask(req.params.id)
    .then((resultFromController) => res.send(resultFromController));
});

// Complete a task
router.put('/:id/complete', (req, res) => {
  taskController.completeTask(req.params.id).then((resultFromController) => res.send(resultFromController));
});
module.exports = router;
