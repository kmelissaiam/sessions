const Task = require('../models/Task.js');

module.exports.getAllTask = () => {
  return Task.find({}).then((result) => {
    return result;
  });
};

module.exports.createTask = (requestBody) => {
  if (requestBody != null && requestBody.name !== '') {
    return Task.findOne({ name: requestBody.name }).then((result) => {
      if (result != null && result.name === requestBody.name) {
        return 'Duplicate Task';
      } else {
        const newTask = new Task(requestBody);
        return newTask.save().then((newTask, error) => {
          if (error) {
            console.log(error);
          } else {
            return newTask;
          }
        });
      }
    });
  } else {
  }
};

module.exports.deleteTask = (taskId) => {
  return Task.findByIdAndDelete(taskId).then((removeTask, error) => {
    if (error) {
      console.log(error);
    } else {
      return removeTask;
    }
  });
};

module.exports.updateTask = (taskId, requestBody) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.log(error);
      return res.send('cannot find data with the provided id');
    }

    result.name = requestBody.name;

    return result.save().then((updatedTask, error) => {
      if (error) {
        console.log(error);
        return res.send('There is an error while saving the update.');
      } else {
        return updatedTask;
      }
    });
  });
};

module.exports.getSpecificTask = (taskId) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.log(error);
      return res.send('cannot find data with the provided id');
    } else {
      return result;
    }
  });
};

module.exports.completeTask = (taskId) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.log(error);
      return res.send('cannot find data with the provided id');
    }

    result.status = 'Completed';

    return result.save().then((updatedTask, error) => {
      if (error) {
        console.log(error);
        return res.send('There is an error while saving the update.');
      } else {
        return updatedTask;
      }
    });
  });
};
