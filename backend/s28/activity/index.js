/*
    Create functions which can manipulate our arrays.
*/

/*
    
   1. Create a function called displayValues() which is able to receive an array of numbers and display cubed values in the console.
        - Use the forEach() method to print the square of each number on a new line.
*/
        function displayValues(num) {
          num.forEach((arr) => {
            console.log(arr ** 3);
          });
        }
        console.log(displayValues([1, 2, 3]));
/*

/*
3. Write a function called areAllEven that takes an array of numbers as input and returns a boolean which determines if all the numbers given are even or not.
    - Create a new variable inside the function called isEven.
    - Use the every() method to check if all the numbers are even and save the result into the variable. 
    - Return the isEven variable.
*/

        function areAllEven(arr) {
          let isEven = arr.every((num) => num % 2 === 0);
          return isEven;
        }
        console.log(areAllEven([2, 4, 6]));

/*
4. Write a function called hasDivisibleBy8 that takes an array of numbers as input returns a boolean which determines if the array contains at least one number divisible by 8.
- Create a new variable called hasDivisible
- Use the some() method to check if at least one of the given values is divisible by 8, save the result in the hasDivisible variable.
- Return the hasDivisible variable.
*/

        function hasDivisibleBy8(arr) {
          let hasDivisible = arr.some((num) => num % 8 === 0);
          return hasDivisible;
        }
        console.log(hasDivisibleBy8([8, 25, 100]));

/*
5. Write a function called filterEvenNumbers that takes an array of numbers as input and returns a new array containing only the even numbers from the original array.
    - Create a new variable called filteredNum
    - Use the filter() method to create a new array of only the even numbers from the original array save it in the filteredNum variable.
    - Return the filteredNum variable.
*/
        function filterEvenNumbers(arr) {
          let filteredNum = arr.filter((num) => num % 2 === 0);
          return filteredNum;
        }
        console.log(filterEvenNumbers([10, 15, 50]));


//Do not modify
//For exporting to test.js
try {
  module.exports = {
    displayValues: typeof displayValues !== 'undefined' ? displayValues : null,
    areAllEven: typeof areAllEven !== 'undefined' ? areAllEven : null,
    hasDivisibleBy8: typeof hasDivisibleBy8 !== 'undefined' ? hasDivisibleBy8 : null,
    celsiusToFahrenheit: typeof celsiusToFahrenheit !== 'undefined' ? celsiusToFahrenheit : null,
    filterEvenNumbers: typeof filterEvenNumbers !== 'undefined' ? filterEvenNumbers : null,
    getProductNames: typeof getProductNames !== 'undefined' ? getProductNames : null,
  };
} catch (err) {}