//$gt greater than
//$gte greater than equal to
//db.table.find({ field: { $gt : value }})

db.users.find({ age: { $gt: 50 } });
db.users.find({ age: { $gte: 50 } });

//$lt less than
//$lte less than equal to
//db.table.find({ field: { $lt : value }})

db.users.find({ age: { $lt: 50 } });
db.users.find({ age: { $lte: 50 } });

//$ne not equal
//db.table.find({ field: { $ne : value }})

db.users.find({ age: { $le: 82 } });

//$in in between values of an array
//db.table.find({ field: { $in : [ value1, value2,] } } )

db.users.find({ lastName: { $in: ['Hawking', 'Doe'] } });
db.users.find({ courses: { $in: ['HTML', 'React'] } });

//$nin not in between values of an array
//db.table.find({ field: { $nin : [ value1, value2,] } } )

db.users.find({ lastName: { $nin: ['Hawking', 'Doe'] } });
db.users.find({ courses: { $nin: ['HTML', 'React'] } });

// Logical Query Operator
// $or at least one operator value must be true to be true
// db.table.find({$or: [{ field: value }, { field: value2 }]});
db.users.find({ $or: [{ firstName: 'Neil' }, { age: 25 }] });

//or with gt

db.users.find({
  $or: [
    {
      firstName: 'Neil',
    },
    {
      age: {
        $gt: 30,
      },
    },
  ],
});

// $and all operator must be true to be true
// db.table.find({$and: [{ field: value }, { field: value2 }]});

db.users.find({
  $and: [
    {
      $age: {
        $ne: 82,
      },
    },
    {
      $age: {
        $ne: 76,
      },
    },
  ],
});

// Field Projection

//Inclusion
/*
    - Allows us to include/add specific fields only when retrieving documents.
    - The value provided is 1 to denote that the field is being included.
    - Syntax
        db.users.find({criteria},{field: 1})
*/

db.users.find(
  {
    firstName: 'Jane',
  },
  {
    firstName: 1,
    lastName: 1,
    contact: 1,
  }
);

//Exclusion
/*
    - Allows us to exclude/remove specific fields only when retrieving documents.
    - The value provided is 1 to denote that the field is being included.
    - Syntax
        db.users.find({criteria},{field: 0})
*/

db.users.find(
  {
    firstName: 'Jane',
  },
  {
    contact: 0,
    department: 0,
  }
);

// selecting a value on an array or an object
// db.users.find({ field: value}, { "field.object": 1 or 0})
db.users.find(
  {
    firstName: 'Jane',
  },
  {
    firstName: 1,
    lastName: 1,
    'contact.email': 1,
    _id: 0,
  }
);

db.users.find(
  {
    firstName: 'Jane',
  },
  {
    firstName: 1,
    lastName: 1,
    'contact.phone': 1,
    _id: 0,
  }
);

// Suppressing the ID Field
db.users.find(
  {
    firstName: 'Jane',
  },
  {
    firstName: 1,
    lastName: 1,
    contact: 1,
    _id: 0,
  }
);

//Evaluation Query Operator

// $regex Operator

/*
SYNTAX:

db.table.find({ field: { $regex : value , $options: value } })

*/

// Case Sensitive Query
db.users.find(
  {
    firstName: { $regex: 'N' },
  },
  {
    firstName: 1,
    lastName: 1,
    _id: 0,
  }
);

// Case Insensitive Query

db.users.find(
  {
    firstName: { $regex: 'n', $options: 'i' },
  },
  {
    firstName: 1,
    lastName: 1,
    _id: 0,
  }
);
