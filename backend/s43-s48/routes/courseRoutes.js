const express = require('express');
// Allows access to HTTP Methods and Middlewares
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require('../auth.js');

// Destructure from auth
let { verify, verifyAdmin } = auth;

// Add Course
router.post('/', verify, verifyAdmin, courseController.addCourse);

// Get Courses
router.get('/all', courseController.getAllCourses);

// Get Active Courses
router.get('/', courseController.getAllActive);

// Get a Specific Course via ID
router.get('/:courseId', courseController.getCourse);

// Updating a specific course (Admin Only)
router.put('/:courseId', verify, verifyAdmin, courseController.updateCourse);

// Archiving a specific active course (Admin Only)
router.put('/:courseId/archive', verify, verifyAdmin, courseController.archiveCourse);

// Activating a specific inactive Course (Admin Only)
router.put('/:courseId/activate', verify, verifyAdmin, courseController.activateCourse);

// Exports
module.exports = router;
