// Dependencies
const mongoose = require('mongoose');

// Schema
const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, 'Please add a first name'],
  },
  lastName: {
    type: String,
    required: [true, 'Please add a last name'],
  },
  email: {
    type: String,
    required: [true, 'Please add an email'],
    unique: true,
  },
  password: {
    type: String,
    required: [true, 'Please add a password'],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  mobileNo: {
    type: String,
    required: [true, 'Please add a mobile number'],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  enrollments: [
    {
      courseId: {
        type: String,
        required: [true, 'Please add a course id'],
      },
      courseName: {
        type: String,
      },
      courseDescription: {
        type: String,
      },
      coursePrice: {
        type: Number,
      },
      enrolledOn: {
        type: Date,
        default: new Date(),
      },
      status: {
        type: String,
        default: 'Enrolled',
      },
    },
  ],
});

// Exports
module.exports = mongoose.model('Users', userSchema);
