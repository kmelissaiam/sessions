const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 3001;

mongoose.connect(
  'mongodb+srv://admin:admin123@cluster0.w7dkija.mongodb.net/B305-to-do?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error!'));
db.once('open', () => console.log("We're connected to the Cloud Database."));

const userSchema = new mongoose.Schema({
  // define fields with corresponding data types
  username: String,
  password: String,
});

const User = mongoose.model('User', userSchema);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post('/signup', (req, res) => {
  if (req.body.username !== '' && req.body.password !== '') {
    User.findOne({ username: req.body.username }).then((result, error) => {
      if (result != null && result.username === req.body.username) {
        return res.send('Duplicate User Found!');
      } else {
        let newUser = new User({
          username: req.body.username,
          password: req.body.password,
        });

        newUser.save().then((savedUser, saveErr) => {
          if (saveErr) {
            return console.error(saveErr);
          } else {
            return res.status(201).send('New User Created!');
          }
        });
      }
    });
  } else {
    res.send('Please fill both username and password field');
    res.end();
  }
});

app.get('/signup', (req, res) => {
  User.find({}).then((result, error) => {
    // If error occured
    if (error) {
      return console.error(error);
    } else {
      return res.status(200).json({
        data: result,
      });
    }
  });
});
// port listening
if (require.main === module) {
  app.listen(port, () => console.log(`Server is running at port ${port}`));
}

module.exports = app;
