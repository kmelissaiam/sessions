const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 3001;

mongoose.connect(
  'mongodb+srv://admin:admin123@cluster0.w7dkija.mongodb.net/B305-to-do?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

// set notification for connection success or failure
let db = mongoose.connection;
// if error occured, output in console
db.on('error', console.error.bind(console, 'Connection Error!'));

// if connection is successful, output in console
db.once('open', () => console.log("We're connected to the Cloud Database."));

// [SECITON] Mongoose Schema
// Schemas determine the structure of the documents to be written in the database

const taskSchema = new mongoose.Schema({
  // define fields with corresponding data types
  name: String,
  status: {
    type: String,
    default: 'Pending',
    // default value is 'Pending'
  },
});

//[SECTION] Models

const Task = mongoose.model('Task', taskSchema);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
    - If the task already exists in the database, we return an error
    - If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

// Task Schema
app.post('/tasks', (req, res) => {
  Task.findOne({ name: req.body.name }).then((result, error) => {
    if (result != null && result.name === req.body.name) {
      return res.send('Duplicate Task Found!');
    } else {
      let newTask = new Task({
        name: req.body.name,
      });

      newTask.save().then((savedTask, saveErr) => {
        if (saveErr) {
          return console.error(saveErr);
        } else {
          return res.status(201).send('New Task Created!');
        }
      });
    }
  });
});

app.get('/tasks', (req, res) => {
  Task.find({}).then((result, error) => {
    // If error occured
    if (error) {
      return console.error(error);
    } else {
      return res.status(200).json({
        data: result,
      });
    }
  });
});

// User Schema

const userSchema = new mongoose.Schema({
  // define fields with corresponding data types
  username: String,
  password: String,
});

const User = mongoose.model('User', userSchema);

app.post('/signup', (req, res) => {
  if (req.body.username !== '' && req.body.password !== '') {
    User.findOne({ username: req.body.username }).then((result, error) => {
      if (result != null && result.username === req.body.username) {
        return res.send('Duplicate User Found!');
      } else {
        let newUser = new User({
          username: req.body.username,
          password: req.body.password,
        });

        newUser.save().then((savedUser, saveErr) => {
          if (saveErr) {
            return console.error(saveErr);
          } else {
            return res.status(201).send('New User Created!');
          }
        });
      }
    });
  } else {
    res.send('Please fill both username and password field');
    res.end();
  }
});

app.get('/users', (req, res) => {
  User.find({}).then((result, error) => {
    // If error occured
    if (error) {
      return console.error(error);
    } else {
      return res.status(200).json({
        data: result,
      });
    }
  });
});

// port listening
if (require.main === module) {
  app.listen(port, () => console.log(`Server is running at port ${port}`));
}

module.exports = app;
