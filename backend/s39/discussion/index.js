// When certain statements take a lot of time to process, this slows down our code
// An example of this are when loops are used on a large amount of information or when fetching data from databases
// When an action will take some time to process, this results in code  "blocking"

console.log('Hello World!');

// for (let i = 0; i <= 1500; i++) {
//   console.log(i);
// }

console.log('Hello Again!');

// [SECTION] Getting all posts
// fetch('URL').then((response) => () {});

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

fetch('https://jsonplaceholder.typicode.com/posts').then((response) => console.log(response.status));

// json() will convert response/object into json format
fetch('https://jsonplaceholder.typicode.com/posts')
  .then((response) => response.json())
  .then((json) => console.log(json));

async function fetchData() {
  let result = await fetch('https://jsonplaceholder.typicode.com/posts');

  console.log(result);

  console.log(typeof result);

  // We can access the content of the "response" by directly accessing it's "body" property
  console.log(result.body);

  let json = result.json();

  console.log(json);
}

fetchData();

// [SECTION] Getting the specific post

fetch('https://jsonplaceholder.typicode.com/posts/1').then((response) => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts/1')
  .then((response) => response.json())
  .then((json) => console.log(json));

// [SECTION] Creating a post

//fetch('URL', options)

fetch('https://jsonplaceholder.typicode.com/posts', {
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({
    title: 'New Post',
    body: 'Hello World!',
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// [SECTION] Updating a post

fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'PUT',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({
    title: 'Updated Post',
    body: 'Hello Again!',
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// [SECTION] Deleting a post

fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'DELETE',
}).then((response) => response.send('Data Deleted'));
