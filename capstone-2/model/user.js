// Dependencies
const mongoose = require('mongoose');

// Schema
const userSchema = new mongoose.Schema(
{
	firstName: {
		type: String,
		required: [true, 'Please add a first name'],
	},

	lastName: {
		type: String,
		required: [true, 'Please add a last name'],
	},

	phoneNumber: {
		type: Number,
		required: [true, 'Please add a phone number'],
	},

	email: {
		type: String,
		required: [true, 'Please add an email'],
		unique: true,
	},

	homeAddress: {
		type: String,
		required: [true, 'Please add your home address'],
	},

	password: {
		type: String,
		required: [true, 'Please add a password'],
	},

	createdOn: {
		type: Date,
		default: new Date(),
	},

	isAdmin: { 
		type: Boolean,
		default: false,
	}
});

module.exports = mongoose.model('Users', userSchema);
