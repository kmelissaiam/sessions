// Dependencies
const mongoose = require('mongoose');

// Schema
const productSchema = new mongoose.Schema(
{
	productName: {
		type: String,
		required: [true, 'Please enter the product name'],
	},

	productDescription: {
		type: String,
		required: [true, 'Please add the product description'],
	},

	price: {
		type: Number,
		required: [true, 'Please add the price'],
	},

	quantity: {
		type: Number,
		required: [true, 'Please add the quantity'],
	},

	isActive: {
		type: Boolean,
		default: true,
	},

	createdOn: {
		type: Date,
		default: new Date(),
	},
	
});

module.exports = mongoose.model('Products', productSchema);
