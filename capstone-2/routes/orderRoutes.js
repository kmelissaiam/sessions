// Dependencies
const express = require('express');
const auth = require('../auth.js');

// Routing Component
const router = express.Router();

// Controller
const orderController = require('../controller/orderController.js');

// De-Structure Auth
const { verify, verifyAdmin } = auth;

// Create a new order
router.post('/placeorder', verify, orderController.createOrder);

// Get all orders (admin access only)
router.get('/all', verify, verifyAdmin, orderController.getAllOrders);

// Get a single order by ID (admin access only)
router.get('/:id/view', verify, verifyAdmin, orderController.getOrderById);

// Update an order by ID
router.put('/:id/update', orderController.updateOrder);

// Delete an order by ID
router.delete('/:id/delete', orderController.deleteOrder);

module.exports = router;