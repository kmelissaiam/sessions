// Dependencies
const express = require('express');
const auth = require('../auth.js');

// Routing Component
const router = express.Router();

// Controller
const userController = require('../controller/userController.js');

// De-Structure Auth
const { verify, verifyAdmin } = auth;

// User Registration End Point
router.post('/register', userController.registerUser);

// User Login
router.post('/login', userController.userLogin);

// Get All Users (for admin)
router.get('/all', verify, verifyAdmin, userController.getAllUsers);

// Register Admin User (requires admin privileges)
router.post('/register-admin', verify, verifyAdmin, userController.registerAdminUser);

// View Admin Users (requires admin privileges)
router.get('/admin', verify, verifyAdmin, userController.viewAdminUsers);

// Export
module.exports = router;