// Dependencies
const jwt = require('jsonwebtoken');
const secret = 'ECOMAPI';

// Create Token
module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  return jwt.sign(data, secret, {});
};

// Retrieve token from the request header
module.exports.verify = (req, res, next) => {
  const token = req.headers.authorization;

  if (!token) {
    return res.status(401).json({ auth: 'Failed, No Token!' });
  }

  if (!token.startsWith('Bearer ')) {
    return res.status(401).json({ auth: 'Failed', message: 'Invalid token format' });
  }

  const tokenValue = token.slice(7); // Remove 'Bearer ' prefix

  jwt.verify(tokenValue, secret, (error, decodedToken) => {
    if (error) {
      console.error(error); // Log the error for debugging
      return res.status(401).json({ auth: 'Failed', message: 'Invalid token' });
    } else {
      req.user = decodedToken;
      console.log(decodedToken); // Log the decoded token payload
      next();
    }
  });
};

// Verify User Level
module.exports.verifyAdmin = (req, res, next) => {
  if (req.user && req.user.isAdmin) { // Check if req.user exists and has isAdmin property
    next(); // User is admin, proceed to the next middleware
  } else {
    return res.status(403).json({
      auth: 'Failed!',
      message: 'Access Forbidden',
    });
  }
};
