// Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

require('dotenv').config();

// Fixed Parameter for .env (3rd Parameter= Port)
process.env.PORT

const port = 4000;

// Access Routes
const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js');
const orderRoutes = require('./routes/orderRoutes.js');

// Middleware (Express should come first)
const app = express();

app.use(cors());
app.use(express.urlencoded({ extended: true}));
app.use(express.json());


// Database Connection
mongoose.connect(process.env.dbLink, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

// Checking of error
const dbChecker = mongoose.connection;
dbChecker.on('error', console.error.bind(console, 'Connection Error: ${dbChecker}'));
dbChecker.once('open', () => {
	console.log('Connected to Atlas Cloud Database');
});

// Back-end routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);


// Gateway
if(require.main == module){
	app.listen(process.env.PORT || port, () =>{
		console.log(`Server is running on port ${process.env.PORT || port}`);
	});
};

// Export App
module.exports = app;