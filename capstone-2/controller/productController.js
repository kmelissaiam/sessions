// Dependencies
const Products = require('../model/product.js');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

// Add Products
const addProduct = async (req, res) => {
  try {
    const newProduct = new Products({
      productName: req.body.name,
      productDescription: req.body.description,
      price: req.body.price,
      quantity: req.body.quantity,
    });

    const product = await newProduct.save();
    res.status(200).json({ message: 'New Product Added', Info: product });
  } catch (err) {
    console.error(err); // Log the error for debugging
    res.status(500).send('Internal Server Error');
  }
};

// Retrieve Active Products
const activeProducts = async (req, res) => {
  try {
    const products = await Products.find({ isActive: true });
    res.status(200).json(products);
  } catch (err) {
    res.status(500).send('Internal Server Error');
  }
};

// Retrieve Single Product
const singleProduct = async (req, res) => {
  try {
    const product = await Products.findOne({ _id: req.params.id, isActive: true });
    res.status(200).json(product);
  } catch (err) {
    res.status(500).send('Internal Server Error');
  }
};

// Update Product
const updateProduct = async (req, res) => {
  try {
    const product = await Products.findOneAndUpdate(
      { _id: req.params.id },
      {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        quantity: req.body.quantity,
      }
    );
    res.status(200).json({ message: 'Product updated successfully!' });
  } catch (err) {
    res.status(500).send('Internal Server Error');
  }
};

// Archive Product
const archiveProduct = async (req, res) => {
  try {
    const product = await Products.findOneAndUpdate({ _id: req.params.id }, { isActive: false });
    res.status(200).json({ message: 'Product archived successfully!' });
  } catch (err) {
    res.status(500).send('Internal Server Error');
  }
};

// Activate Product
const activateProduct = async (req, res) => {
  try {
    const product = await Products.findOneAndUpdate({ _id: req.params.id }, { isActive: true });
    res.status(200).json({ message: 'Product activated successfully!' });
  } catch (err) {
    res.status(500).send('Internal Server Error');
  }
};

module.exports = {
  addProduct,
  activeProducts,
  singleProduct,
  updateProduct,
  archiveProduct,
  activateProduct,
};
