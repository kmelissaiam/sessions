// Dependencies
const Order = require('../model/order.js');
const Products =require('../model/product.js');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

// Create a new order
const createOrder = async (req, res) => {
  try {
    const user = req.user;
    console.log('User: ', user);

    if (!user) {
      return res.status(401).json({ error: 'Unauthorized' });
    }

    const productId = req.body.productId;
    const quantity = req.body.quantity || 1;

    const product = await Products.findById(productId);

    if (!product) {
      return res.status(404).json({ error: 'Product not found' });
    }

    
    const totalAmount = product.price * quantity;

    const newOrder = new Order({
      userId: user.id,
      productId: productId,
      quantity: quantity,
      totalAmount: totalAmount,
    });

    const order = await newOrder.save();

    if (order) {
      return res.status(201).json({ message: 'Order Created Successfully!', data: order });
    } else {
      return res.status(500).json({ error: 'Failed to create order' });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


// Get all orders (accessible only to admin users)
const getAllOrders = async (req, res) => {
  try {
    // Check if the user making the request is an admin
    if (!req.user.isAdmin) {
      return res.status(403).json({ success: false, error: 'Access denied' });
    }

    // Assuming you have an Order model
    const orders = await Order.find();

    return res.status(200).json({ success: true, data: orders });
  } catch (err) {
    console.error(err);
    res.status(500).json({ success: false, error: 'Internal Server Error' });
  }
};

// Get a single order by ID (admin access only)
const getOrderById = async (req, res) => {
  try {
    // Check if the user making the request is an admin
    if (!req.user.isAdmin) {
      return res.status(403).json({ success: false, error: 'Access denied' });
    }

    const order = await Order.findById(req.params.id);

    if (!order) {
      return res.status(404).json({ success: false, error: 'Order not found' });
    }

    return res.status(200).json({ success: true, data: order });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ success: false, error: error.message });
  }
};

// Update an order by ID
const updateOrder = async (req, res) => {
  try {
    const orderId = req.params.id;
    const { quantity } = req.body; // Get the updated quantity from the request body

    const order = await Order.findById(orderId);

    if (!order) {
      return res.status(404).json({ success: false, error: 'Order not found' });
    }

    // Get the product associated with this order
    const product = await Products.findById(order.productId);

    if (!product) {
      return res.status(404).json({ success: false, error: 'Product not found' });
    }

    // Update the quantity in the order
    order.quantity = quantity;

    // Recalculate the totalAmount based on the updated quantity
    order.totalAmount = product.price * quantity;

    // Save the updated order
    const updatedOrder = await order.save();

    return res.status(200).json({ success: true, data: updatedOrder });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ success: false, error: error.message });
  }
};

// Delete an order by ID
const deleteOrder = async (req, res) => {
  try {
    // Assuming you have an Order model
    const order = await Order.findByIdAndDelete(req.params.id);

    if (!order) {
      return res.status(404).json({ success: false, error: 'Order not found' });
    }

    return res.status(204).json({ success: true, data: null });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ success: false, error: error.message });
  }
};

module.exports = {
  createOrder,
  getAllOrders,
  getOrderById,
  updateOrder,
  deleteOrder,
};