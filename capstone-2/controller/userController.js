const Users = require('../model/user.js');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

// Register User
const registerUser = async (req, res) => {
  try {
    const hashPassword = await bcrypt.hash(req.body.password, 10);
    const newUser = new Users({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      phoneNumber: req.body.phoneNumber,
      email: req.body.email,
      homeAddress: req.body.homeAddress,
      password: hashPassword,
    });

    newUser
      .save()
      .then(() => {
        res.status(201).send('User Created Successfully!');
      })
      .catch((err) => {
        console.error(err);
        res.status(500).send('Internal Server Error');
      });
  } catch (err) {
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
};

// User Login
const userLogin = async (req, res) => {
  try {
    const user = await Users.findOne({ email: req.body.email });
    console.log("User found:", user); // Check if user is found

    if (user === null) {
      return res.status(401).send('Invalid Credentials. Please Try Again!');
    }

    const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password);
    console.log("Password correct:", isPasswordCorrect); // Check if password is correct

    if (isPasswordCorrect) {
      const accessToken = auth.createAccessToken(user);
      return res.json({ accessToken });
    } else {
      return res.status(401).send('Invalid Credentials. Please Try Again!');
    }
  } catch (err) {
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
};

// Get All Users (for admin)
const getAllUsers = async (req, res) => {
  try {
    const users = await Users.find();

    return res.status(200).json({ success: true, data: users });
  } catch (err) {
    console.error(err);
    res.status(500).json({ success: false, error: 'Internal Server Error' });
  }
};

// Register Admin User (requires admin privileges)
const registerAdminUser = async (req, res) => {
  try {
    // Check if the email already exists in the database
    const existingUser = await Users.findOne({ email: req.body.email });
    if (existingUser) {
      return res.status(400).json({ error: 'E-mail already exists' });
    }

    // Create a new admin user
    const newUser = new Users({
      email: req.body.email,
      password: await bcrypt.hash(req.body.password, 10),
      isAdmin: true,
    });

    // Save the new user to the database
    await newUser.save();

    // Respond with a success message
    return res.status(201).json({ message: 'Admin User Registered Successfully' });
  } catch (err) {
    console.error(err);
    return res.status(500).json({ error: 'Internal Server Error' });
  }
};

// View Admin Users (requires admin privileges)
const viewAdminUsers = async (req, res) => {
  try {
    const adminUsers = await Users.find({ isAdmin: true });

    return res.status(200).json({ success: true, data: adminUsers });
  } catch (err) {
    console.error(err);
    res.status(500).json({ success: false, error: 'Internal Server Error' });
  }
};

module.exports = {
  registerUser,
  userLogin,
  getAllUsers,
  registerAdminUser,
  viewAdminUsers,
};